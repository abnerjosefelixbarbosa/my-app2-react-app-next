export interface Person {
    id: Number;
    name: String;
}