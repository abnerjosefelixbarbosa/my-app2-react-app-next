import { MainProvider } from '@/contexts/useMain'
import '@/styles/globals1.css'
import type { AppProps } from 'next/app'

export default function App({ Component, pageProps }: AppProps) {
  return (
  <MainProvider>
    <Component {...pageProps} />
  </MainProvider>
  )
}
