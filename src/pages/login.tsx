"use client"

import useMain from '@/contexts/useMain';
import styles from '@/styles/Login.module.css'
import { useRouter } from 'next/navigation'
import { useEffect } from 'react';

export default function Login() {
    const router = useRouter();
    const { person } = useMain();

    useEffect(() => {
        console.log(person)
    }, []);


    function handleIndex() {
        router.push("/")
    }

    return (
        <div className={styles.body}>
          <h1>login</h1>
          <button onClick={handleIndex}>index</button>
        </div>
    )
}