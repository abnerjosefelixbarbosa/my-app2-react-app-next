"use client";

import { Inter } from "next/font/google";
import styles from "@/styles/Home.module.css";
import { useRouter } from "next/navigation";
import useMain from "@/contexts/useMain";

const inter = Inter({ subsets: ["latin"] });

export default function Home() {
  const router = useRouter();
  const { setPerson } = useMain();

  function handleLogin() {
    setPerson({ id: 1, name: "name1" });
    router.push("/login");
  }

  return (
    <div className={styles.body}>
      <h1>index</h1>
      <button onClick={handleLogin}>Login</button>
    </div>
  );
}
